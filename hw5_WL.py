import numpy as np
from numpy import *
import math
import numpy.random
from sklearn.datasets import fetch_mldata
import sklearn.preprocessing

mnist = fetch_mldata('MNIST original')
data = mnist['data']
labels = mnist['target']

neg, pos = 0,8
train_idx = numpy.random.RandomState(0).permutation(where((labels[:60000] == neg) | (labels[:60000] == pos))[0])
test_idx = numpy.random.RandomState(0).permutation(where((labels[60000:] == neg) | (labels[60000:] == pos))[0])

train_data_size = 2000
train_data_unscaled = data[train_idx[:train_data_size], :].astype(float)
train_labels = (labels[train_idx[:train_data_size]] == pos)*2-1

#validation_data_unscaled = data[train_idx[6000:], :].astype(float)
#validation_labels = (labels[train_idx[6000:]] == pos)*2-1

test_data_size = 2000
test_data_unscaled = data[60000+test_idx[:test_data_size], :].astype(float)
test_labels = (labels[60000+test_idx[:test_data_size]] == pos)*2-1

# Preprocessing
train_data = sklearn.preprocessing.scale(train_data_unscaled, axis=0, with_std=False)
#validation_data = sklearn.preprocessing.scale(validation_data_unscaled, axis=0, with_std=False)
test_data = sklearn.preprocessing.scale(test_data_unscaled, axis=0, with_std=False)

def h1(theta, j, x):
    if x[j] > theta:  # left hypothesis
        return -1
    else:
        return 1

def h2(theta, j, x):
    if x[j] > theta:  # right hypothesis
        return 1
    else:
        return -1



def ERM_DS(train_data, train_labels, dist):
    f_min = float("inf")
    d = len(train_data[0]) # sample's dimension
    m = len(train_data) # training set size
    x = np.array(train_data)
    y = np.array(train_labels)
    best_theta = x[0][0]-1
    best_j = 0
    for j in range(d):
        inds = x[:, j].argsort()
        x = x[inds]
        y = y[inds]
        dist_new = []
        for i in inds:
            dist_new.append(dist[i])
        dist = dist_new
        # s = zip(x,y,dist)
        # s = sorted(s, key = lambda sample : sample[0][:,j])
        # x, y, dist = zip(*s)
        # y = y[inds]
        f = 0
        for i in range(m):
            if y[i] == 1:
                f += dist[i]
        if f < f_min:
            f_min = f
            best_theta = x[0][j]-1
            best_j = j
        for i in range(m):
            f = f-y[i]*dist[i]
            if (i != (m-1)) and (f < f_min) and (x[i][j] != x[i+1][j]):
                f_min = f
                best_theta = (x[i][j] + x[i+1][j])/2
                best_j = j
    return best_theta, best_j

def WL(D, train_data, train_labels):
    theta, j = ERM_DS(train_data, train_labels, D)
    h_1, h_2 = 0, 0
    loss1, loss2 = 0, 0
    xs = train_data
    ys = train_labels

    for x, y in zip(xs, ys):
        h_1 = h1(theta, j, x)
        h_2 = h2(theta, j, x)
        if h_1 != y:
            loss1 += 1
        if h_2 != y:
            loss2 += 1
    loss1 = loss1/len(xs)
    loss2 = loss2 / len(xs)

    if loss1 < loss2:
        return lambda func: h1(theta, j, func)
    else:
        return lambda func: h2(theta, j, func)