from numpy import *
import numpy.random
from sklearn.datasets import fetch_mldata
import sklearn.preprocessing
from ml_utils import plot
from hw5_WL import WL

class AdaBoost:

    def __init__(self):
        self.train_data, self.train_labels = None, None
        self.test_data, self.test_labels = None, None
        self.init_data_set()
        self.H, self.W = [], []

    def init_data_set(self):
        """
        Initialize dataset.
        :return: None
        """
        mnist = fetch_mldata('MNIST original')
        data = mnist['data']
        labels = mnist['target']

        neg, pos = 0, 8
        train_idx = numpy.random.RandomState(0).permutation(where((labels[:60000] == neg) | (labels[:60000] == pos))[0])
        test_idx = numpy.random.RandomState(0).permutation(where((labels[60000:] == neg) | (labels[60000:] == pos))[0])

        train_data_size = 2000
        train_data_unscaled = data[train_idx[:train_data_size], :].astype(float)
        self.train_labels = (labels[train_idx[:train_data_size]] == pos) * 2 - 1

        # validation_data_unscaled = data[train_idx[6000:], :].astype(float)
        # validation_labels = (labels[train_idx[6000:]] == pos)*2-1

        test_data_size = 2000
        test_data_unscaled = data[60000 + test_idx[:test_data_size], :].astype(float)
        self.test_labels = (labels[60000 + test_idx[:test_data_size]] == pos) * 2 - 1

        # Preprocessing
        self.train_data = sklearn.preprocessing.scale(train_data_unscaled, axis=0, with_std=False)
        # validation_data = sklearn.preprocessing.scale(validation_data_unscaled, axis=0, with_std=False)
        self.test_data = sklearn.preprocessing.scale(test_data_unscaled, axis=0, with_std=False)

    def classifier(self, x):
        """
        Given a sample X, predict its label.
        :param x: sample
        :return: prediction (-1 / 1)
        """
        s = 0
        for t in range(len(self.W)):
            w, h = self.W[t], self.H[t]
            s += (w * h(x))
        clf = numpy.sign(s)
        return 1 if clf==0 else clf

    def calc_epsilon(self, D):
        e = 0
        m = len(D)
        h = self.H[-1]
        for i in range(m):
            g = h(self.train_data[i])
            if g != self.train_labels[i]:
                e += D[i]
        return e

    def updateD(self, D):
        m = len(D);
        w = self.W[-1]
        h = self.H[-1]
        Dnew = []
        z = 0.0
        for i in range(m):
            x, y = self.train_data[i], self.train_labels[i]
            e = numpy.exp(-w * y * h(x))
            Dnew.append(D[i] * e)
            z += Dnew[-1]
        return [d/z for d in Dnew]

    def boost(self, T):
        m = len(self.train_data)
        D = [1.0/m] * m
        T = [i+1 for i in range(T)]
        train_errors = []
        test_errors = []
        for t in T:
            print("Iteration #{0} of {1}".format(t,len(T)))
            h = WL(D, self.train_data, self.train_labels)
            self.H.append(h)
            e = self.calc_epsilon(D)
            w = 0.5 * numpy.log((1 / e) - 1)
            self.W.append(w)
            D = self.updateD(D)
            train_errors.append(self.calc_error())
            test_errors.append(self.calc_error(test=True))
        plot('Q1b.png', T, [train_errors, test_errors], title='Train & Test Errors for AdaBoost',
             xlabel='Iteration', ylabel='Error Rate', legends=['Train error', 'Test error'])

    def calc_error(self, test=False):
        xs, ys = None, None
        if test:
            xs, ys = self.test_data, self.test_labels
        else:
            xs, ys = self.train_data, self.train_labels
        m = len(xs)
        error = 0.0
        for i in range(m):
            if self.classifier(xs[i]) != ys[i]:
                error += 1
        return error / m

booster = AdaBoost()
booster.boost(25)